package service;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import modelo.Cliente;
import repository.ClienteRepository;

/* Ejemplo de capa service del dominio del cliente (usuario). 
  Requiere modificar la tabla cliente añadiendo
  el campo username
  	alter table cliente
		add column username varchar(20) unique after direccion;

    update cliente 
		set username = concat("usr", id),
			passwd = sha1('1234');
 */
/**
 * Capa service del dominio de clientes
 */
public class ClienteService {
	ClienteRepository clienteRepository;

	public ClienteService() throws SQLException {
		clienteRepository = new ClienteRepository();

	}

	// Comprobar si ya existe el username.
	// Si existe: abortar (exception)
	// Sino, realiza inserción
	public void registraCliente(Cliente cli) throws Exception {

	}

	// Comprobar si existe el username
	// Si existe, comprueba password.
	// Sino, abortar (exception)
	public Cliente loginCliente(String username, String password) throws Exception {
		return null;
	};

	// Eliminar cliente
	public void bajaCliente(Cliente cli) throws Exception {

	}

	// Cambio de la contraseña del cliente (id o username)
	public void cambiaContrasenya(int idCliente, String password) throws Exception {

	}

	// Cambia los datos del perfil del usuario: nombre, direccion,...
	// Se debe comprobar existencia.
	public void cambiaPerfil(Cliente cli) throws Exception {

	}

	// Obtención de un cliente
	// Si no encuentra cliente lanza exception.
	public Cliente obtenerCliente(int idCliente) throws Exception {
		return null;
	}

	// Busca clientes por aproximación de nombre
	// Si no encuentra ninguno lanza exception.
	public List<Cliente> buscaPorNombre(String nombre) throws Exception {
		List<Cliente> listResultado = new ArrayList<Cliente>();
		return listResultado;
	}

	// Carga facturas del cliente (sin paginación)
	public void cargaFacturas(Cliente cliente) throws Exception {
		
	}

	public double calculaTotalFacturado(int clienteId) throws Exception {
		return clienteRepository.calculaTotalFacturado(clienteId);
	}

}
