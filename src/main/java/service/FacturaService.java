package service;

import java.util.ArrayList;
import java.util.List;

import modelo.Factura;
import repository.FacturaRepository;
import repository.LineaFacturaRepository;

/**
 * Capa service del dominio de facturas
 */
public class FacturaService {

	FacturaRepository facturaRepository;
	LineaFacturaRepository linFacturaRepository;

	public FacturaService() throws Exception {
		facturaRepository = new FacturaRepository();
		linFacturaRepository = new LineaFacturaRepository();

	}

	// Permite obtener una factura a partir de su id (con o sin lineas)
	// Si no encuentra factura lanza exception.
	public Factura obtenerFactura(int id, boolean conLineas) throws Exception {
		Factura factura = facturaRepository.get(id, conLineas);
		if (factura != null) {
			return factura;
		} else {
			throw new Exception("Ha habido un problema obtenidendo factura. Probablemente no existe.");
		}
	}

	// Permite obtener el número de páginas disponible
	public long obtenerNumPaginas() throws Exception {
		return facturaRepository.getNumPages();
	}

	// Permite obtener las facturas de la página indicada (con o sin lineas)
	// Si no encuentra ninguna lanza exception.
	public List<Factura> obtenerFacturas(int pagina, boolean conLineas) throws Exception {
		List<Factura> listResultado = new ArrayList<Factura>();
		return listResultado;
	}

	// Inserta o actualiza una factura. Tendrá en cuenta que una factura puede tener
	// lineas. Además, actualizará stock. Todo en modo transaccional.
	public void guardaFactura(Factura factura) throws Exception {

	}

	// Permite eliminar la factura que se le pase como parámetro. Eliminará factura
	// y sus lineas asociadas.
	// Todo en modo transaccional.
	public void eliminaFactura(Factura factura) throws Exception {
		
	}

}
