package service;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import modelo.Articulo;
import repository.ArticuloRepository;
import vista.ConexionHB;

/**
 * Capa service del dominio de articulos
 */
public class ArticuloService {

	ArticuloRepository articuloRepository;

	public ArticuloService() throws SQLException {
		articuloRepository = new ArticuloRepository();
	}

	// Obtener artículo completo con los datos del grupo
	// Si no encuentra artículo lanza exception.
	public Articulo obtenerArticulo(int art) throws Exception {
		Articulo articulo = articuloRepository.get(art);
		if (articulo != null) {
			return articulo;
		} else {
			throw new Exception("Ha habido un problema obteniendo articulo. Probablemente no existe");
		}
	}

	// Obtener todos los artículos
	// Si no encuentra ninguno lanza exception.
	public List<Articulo> obtenerArticulos() throws Exception {
		List<Articulo> listResultado = new ArrayList<Articulo>();
		return listResultado;
	}

	// Alta de un artículo nuevo
	public void altaArticulo(Articulo articulo) throws Exception {

	}

	// Modifica datos de un artículo
	// Se debe comprobar existencia. Si no existe el artículo se lanza exception.
	public void modificaArticulo(Articulo articulo) throws Exception {

	}

	// Actualiza el stock de un artículo
	// Se debe comprobar existencia. Si no existe el artículo se lanza exception.
	public void actualizaStock(int articulo, int nuevoStock) throws Exception {

	}

	// Baja de un artículo
	public void eliminaArticulo(Articulo articulo) throws Exception {
		try {
			ConexionHB.beginTransaction();
			articuloRepository.delete(articulo);
			ConexionHB.commit();
		} catch (Exception e) {
			throw new Exception("Error al intentar eliminar artículo " + e.getMessage());
		}
	}

	// Busca articulos por aproximación de nombre
	// Si no encuentra ninguno lanza exception.
	public List<Articulo> buscaPorNombre(String nombre) throws Exception {
		List<Articulo> listResultado = new ArrayList<Articulo>();
		return listResultado;
	}

	// Busca articulos por aproximación de nombre y hasta precio
	// Si no encuentra ninguno lanza exception.
	public List<Articulo> buscaPorNombreHastaPrecio(String nombre, float precio) throws Exception {
		List<Articulo> listResultado = new ArrayList<Articulo>();
		return listResultado;
	}

	// Busca articulos por grupo
	// Si no encuentra ninguno lanza exception.
	public List<Articulo> buscaPorGrupo(int grupo) throws Exception {
		List<Articulo> listResultado = new ArrayList<Articulo>();
		return listResultado;
	}

	// Busca articulos por aproximación de nombre y entre precios
	// Si no encuentra ninguno lanza exception.
	public List<Articulo> buscaPorNombreEntrePrecios(String nombre, float precioDesde, float precioHasta)
			throws Exception {
		List<Articulo> listResultado = new ArrayList<Articulo>();
		return listResultado;
	}

}
