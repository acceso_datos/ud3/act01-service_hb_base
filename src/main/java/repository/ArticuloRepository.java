package repository;

import java.util.List;

import org.hibernate.Session;

import modelo.Articulo;
import vista.ConexionHB;

public class ArticuloRepository {
	Session sesion;

	public ArticuloRepository() {
		sesion = ConexionHB.getSession();
	}

	public Articulo get(int id) {
		return sesion.find(Articulo.class, id);
	}

	public List<Articulo> getAll() {
		return null;
	}

	public boolean exists(int id) {
		return true;
	}

	public void save(Articulo articulo) {		
	}

	public void delete(Articulo articulo) {
		sesion.remove(articulo);
	}

	public List<Articulo> getByName(String nombre) {
		return null;
	}

	public List<Articulo> getByNamePrice(String nombre, float precio) {
		return null;
	}

	public List<Articulo> getByGroup(int grupo) {
		return null;
	}

	public List<Articulo> getByNameBetweenPrices(String nombre, float precioDesde, float precioHasta) {
		return null;
	}

	public void close() {
		sesion.close();
	}

}
