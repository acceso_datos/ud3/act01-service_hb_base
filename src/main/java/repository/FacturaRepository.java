package repository;

import java.sql.SQLException;
import java.util.List;

import org.hibernate.Session;

import modelo.Factura;
import vista.ConexionHB;

public class FacturaRepository {
	private int pageSize = 20;
	Session sesion;

	public FacturaRepository() {
		sesion = ConexionHB.getSession();
	}

	public Factura get(int id, boolean conLineas) {
		Factura factura = sesion.find(Factura.class, id);
		if (factura != null && conLineas) {
			factura.getLinfacturas();
		}
		return factura;
	}	

	public List<Factura> getPage(int pagina, boolean conLineas) {
		return null;
	}

	public boolean exists(int id) throws SQLException {
		return true;
	}

	public void save(Factura factura) {
		
	}

	public void delete(Factura factura) {
		sesion.remove(factura);
	}

	// Se debe usar una query: select count...
	// y hacer el calculo teniendo en cuenta el paseSize
	public long getNumPages() {
		return 0;
	}

	public void setPageSize(int pageSize) {
		this.pageSize = pageSize;
	}

	public void close() throws SQLException {
		sesion.close();
	}
}
