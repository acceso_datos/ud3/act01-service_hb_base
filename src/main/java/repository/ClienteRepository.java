package repository;

import java.util.List;

import org.hibernate.Session;

import modelo.Cliente;
import vista.ConexionHB;

public class ClienteRepository {
	Session sesion;

	public ClienteRepository() {
		sesion = ConexionHB.getSession();
	}

	public Cliente get(int id) {
		return sesion.find(Cliente.class, id);
	}

	public Cliente getByUsername(String userName) {
		return null;
	}

	public boolean exists(int id) {
		return true;
	}

	public boolean existsByUsername(String userName) {
		return true;
	}

	public void save(Cliente cliente) {		
	}

	public void delete(Cliente cliente) {
	}

	public List<Cliente> getByName(String nombre) {
		return null;
	}

	public void loadFacturas(Cliente cliente) {
		
	}

	public double calculaTotalFacturado(int clienteId) {
		return 0;
	}

	public void close() {
		sesion.close();
	}
}
