package repository;

import java.sql.SQLException;

import org.hibernate.Session;

import modelo.Factura;
import modelo.Linfactura;
import vista.ConexionHB;

public class LineaFacturaRepository {
	Session sesion;
	FacturaRepository facturaRepository;

	public LineaFacturaRepository() {
		sesion = ConexionHB.getSession();
		facturaRepository = new FacturaRepository();
	}

	public Linfactura get(int lin, int factura) {
		return null;
	}

	public boolean exists(int lin, int factura) {
		return true;
	}

	// No hace falta hacer la actualización: siempre será nueva linea
	// No olvidar hacer actualización stock.
	public Linfactura save(Linfactura lineaFac) {
		return null;		
	}

	// Realizará borrado y actualización de stock.
	public void delete(Linfactura linea) {
	
	}

	// Borrará todas las lineas de la factura: consulta las lineas
	// y borrarlas una a una.
	public void deleteAllLines(Factura factura) {
		
	}

	public void close() throws SQLException {
		sesion.close();
	}
}
