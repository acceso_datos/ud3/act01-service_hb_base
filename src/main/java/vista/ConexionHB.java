package vista;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;

public class ConexionHB {
	// A SessionFactory is set up once for an application!
	private static Session sesion;

	public static Session getSession() {
		// configure() -> configures settings from hibernate.cfg.xml
		if ((sesion == null) || (!sesion.isOpen())) {
			StandardServiceRegistry registry = new StandardServiceRegistryBuilder().configure().build();
			try {
				SessionFactory sessionFactory = new MetadataSources(registry).buildMetadata().buildSessionFactory();
				sesion = sessionFactory.openSession();
			} catch (Exception e) {
				// The registry would be destroyed by the SessionFactory,
				// but we had trouble building the SessionFactory -> so destroy it manually.
				System.err.println("Error construyendo la sesión...");
				StandardServiceRegistryBuilder.destroy(registry);
			}
		}
		return sesion;
	}

	public static void closeSession() {
		if (sesion != null) {
			sesion.close();
			sesion = null;
		}
	}

	public static void beginTransaction() {
		sesion.getTransaction().begin();
	}

	public static void commit() {
		sesion.getTransaction().commit();
	}

	public static void rollback() {
		sesion.getTransaction().rollback();

	}
}
