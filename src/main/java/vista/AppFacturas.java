package vista;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import modelo.Articulo;
import modelo.Cliente;
import modelo.Factura;
import modelo.Linfactura;
import modelo.Vendedor;
import service.ArticuloService;
import service.ClienteService;
import service.FacturaService;

public class AppFacturas {
	static FacturaService facturaServicio = null;
	static ClienteService clienteServicio = null;
	static ArticuloService articuloServicio = null;

	public static void main(String[] args) {

//		@SuppressWarnings("unused")
		java.util.logging.Logger.getLogger("org.hibernate").setLevel(java.util.logging.Level.SEVERE);
		casosUsoFacturas();

	}

	private static void casosUsoFacturas() {
		try {
			facturaServicio = new FacturaService();
			clienteServicio = new ClienteService();
			articuloServicio = new ArticuloService();
		} catch (Exception e) {
			System.out.println("Error: " + e.getMessage());
		}

		if (facturaServicio != null) {
			ejemploObtenerFacturas(); 
			ejemploObtenerFactura(); 

			ejemploNuevaFactura(); 
			ejemploModificarFactura();
			ejemploEliminaFactura(); 

		}
	}

	private static void ejemploObtenerFacturas() {
		try {
			List<Factura> lista;
			for (int i = 1; i <= facturaServicio.obtenerNumPaginas(); i++) {
				System.out.println();
				System.out.println("***** Página " + i + " *****");
				lista = facturaServicio.obtenerFacturas(i, true);
				for (Factura factura : lista) {
					System.out.println(factura);
					for (Linfactura lin : factura.getLinfacturas()) {
						System.out.println("\t" + lin);
					}
				}
			}
		} catch (Exception e) {
			e.getMessage();
		}
	}

	private static void ejemploObtenerFactura() {
		Factura factura;
		try {
			factura = facturaServicio.obtenerFactura(4, true);
			System.out.println(factura);
			for (Linfactura lin : factura.getLinfacturas()) {
				System.out.println("\t" + lin);
			}
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
	}

	private static void ejemploNuevaFactura() {
		try {
			System.out.println("Creando nueva factura");

			Cliente cliente = clienteServicio.obtenerCliente(1);
			// no tenemos Vendedor_service ni repository hecho (lo obtenemos directamente)
			Vendedor vendedor = ConexionHB.getSession().get(Vendedor.class, 1);
			Articulo articulo1 = articuloServicio.obtenerArticulo(1);
			Articulo articulo2 = articuloServicio.obtenerArticulo(2);
			Articulo articulo5 = articuloServicio.obtenerArticulo(5);

			Factura factura = new Factura(cliente, vendedor, new Date(), "tarjeta");
			List<Linfactura> lineas = new ArrayList<Linfactura>();
			lineas.add(new Linfactura(0, 0, articulo1, 10));
			lineas.add(new Linfactura(0, 0, articulo2, 20));
			lineas.add(new Linfactura(0, 0, articulo5, 12));
			factura.setLinfacturas(lineas);

			facturaServicio.guardaFactura(factura);
			System.out.println("Factura insertada correctamente: ");
			System.out.println(factura);

		} catch (Exception e) {
			System.out.println(e.getMessage());
		}

	}

	private static void ejemploModificarFactura() {

		try {
			int facturaId = 4999;
			System.out.print("Modificando factura...");
			Factura factura = facturaServicio.obtenerFactura(facturaId, true);
			System.out.println(factura);

			// cambiamos cliente
			Cliente cliente = clienteServicio.obtenerCliente(2);
			factura.setCliente(cliente);

			// cambiamos forma de pago
			factura.setFormapago("Otra");

			// hacemos varios cambios en las lineas de la factura
			List<Linfactura> lineas = factura.getLinfacturas();
			// modificamos una linea
			Linfactura lin = lineas.get(1);
			lin.setCantidad(49);
			// borramos una linea
			lineas.remove(0);
			// añadimos otra linea
			Articulo articulo = articuloServicio.obtenerArticulo(4);
			lineas.add(new Linfactura(0, 0, articulo, 10));

			facturaServicio.guardaFactura(factura);
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}

	}

	private static void ejemploEliminaFactura() {
		try {
			int facturaId = 4999;
			System.out.print("Eliminando factura...");
			Factura factura = facturaServicio.obtenerFactura(facturaId, true);
			System.out.println(factura);
			facturaServicio.eliminaFactura(factura);
			System.out.println("Factura eliminada correctamente");
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}

	}

}
