package vista;

import java.util.List;

import modelo.Articulo;
import modelo.Grupo;
import service.ArticuloService;

public class AppArticulos {
	static ArticuloService articuloServicio = null;

	public static void main(String[] args) {
//		@SuppressWarnings("unused")
		java.util.logging.Logger.getLogger("org.hibernate").setLevel(java.util.logging.Level.SEVERE);
		casosUsoArticulos();

	}

	private static void casosUsoArticulos() {
		try {
			articuloServicio = new ArticuloService();
		} catch (Exception e) {
			System.out.println("Error: " + e.getMessage());
		}

		if (articuloServicio != null) {
			ejemploObtenerInfoArticulo();
			ejemploObtenerTodosArticulos();

			ejemploNuevoArticulo();
			ejemploModificaArticulo();
			ejemploActualizaStock();
			ejemploEliminaArticulo();

			ejemploBusquedaPorNombre();
			ejemploBusquedaPorNombreHastaPrecio();
			ejemploObtenerArticulosGrupo();
			ejemploBusquedaPorNombreEntrePrecios();
		}
	}

	private static void ejemploEliminaArticulo() {
		Articulo articulo;
		try {
			int articuloId = 10;
			articulo = articuloServicio.obtenerArticulo(articuloId);
			articuloServicio.eliminaArticulo(articulo);
			System.out.println("Articulo eliminado correctamente");
		} catch (Exception e) {
			System.out.println("Imposible eliminar articulo: " + e.getMessage());
		}

	}

	private static void ejemploActualizaStock() {
		try {
			int articuloId = 1;
			articuloServicio.actualizaStock(articuloId, 12);
			System.out.println("Stock articulo actualizado correctamente");
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
	}

	private static void ejemploObtenerInfoArticulo() {
		try {
			Articulo articulo = articuloServicio.obtenerArticulo(1);
			System.out.println(articulo);
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
	}

	private static void ejemploObtenerTodosArticulos() {
		System.out.println("Listado de todos los artículos (info completa)");
		try {
			List<Articulo> articulos = articuloServicio.obtenerArticulos();
			for (Articulo a : articulos) {
				System.out.println(a);
			}
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
	}

	private static void ejemploNuevoArticulo() {
		Articulo nuevoArt = new Articulo("Impresora", 100f, "impX", 10, new Grupo(1, null));
		try {
			articuloServicio.altaArticulo(nuevoArt);
			System.out.println("Alta correcta: " + nuevoArt);
		} catch (Exception e) {
			System.out.println("Error en alta de articulo: " + e.getMessage());
		}
	}

	private static void ejemploModificaArticulo() {
		try {
			int articuloId = 1;
			Articulo articulo = articuloServicio.obtenerArticulo(articuloId);
			articulo.setPrecio(articulo.getPrecio() * 1.15f);
			articuloServicio.modificaArticulo(articulo);
			System.out.println("Modificación satisfactoria");
		} catch (Exception e) {
			System.out.println("Error modificando articulo: " + e.getMessage());
		}
	}

	private static void ejemploBusquedaPorNombreHastaPrecio() {
		System.out.println("Listado de monitores con precio hasta 200€");
		try {
			List<Articulo> articulos = articuloServicio.buscaPorNombreHastaPrecio("monitor", 200f);
			for (Articulo a : articulos) {
				System.out.println(a);
			}
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}

	}

	private static void ejemploObtenerArticulosGrupo() {
		int grupoId = 2;
		System.out.println("Listado de articulos del grupo " + grupoId);
		try {

			List<Articulo> articulos = articuloServicio.buscaPorGrupo(grupoId);
			for (Articulo a : articulos) {
				System.out.println(a);
			}
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}

	}

	private static void ejemploBusquedaPorNombre() {
		System.out.println("Listado de monitores");
		try {
			List<Articulo> articulos = articuloServicio.buscaPorNombre("monitor");
			for (Articulo a : articulos) {
				System.out.println(a);
			}
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
	}

	private static void ejemploBusquedaPorNombreEntrePrecios() {
		System.out.println("Listado de monitores entre 100 y 200");
		try {
			List<Articulo> articulos = articuloServicio.buscaPorNombreEntrePrecios("monitor", 100f, 200f);
			for (Articulo a : articulos) {
				System.out.println(a);
			}
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}

	}

}
