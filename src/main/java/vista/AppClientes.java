package vista;

import java.util.List;
import java.util.Set;

import modelo.Cliente;
import modelo.Factura;
import service.ClienteService;

public class AppClientes {
	static ClienteService clienteServicio = null;

	public static void main(String[] args) {
//		@SuppressWarnings("unused")
		java.util.logging.Logger.getLogger("org.hibernate").setLevel(java.util.logging.Level.SEVERE);
		casosUsoClientes();

	}

	private static void casosUsoClientes() {
		try {
			clienteServicio = new ClienteService();
		} catch (Exception e) {
			System.out.println("Error: " + e.getMessage());
		}

		if (clienteServicio != null) {
			ejemploLogin();
			ejemploCambioContrasenya();
			ejemploCambioPerfil();
			ejemploRegistro();
			ejemploBusquedaPorNombre();
			ejemploMostrarTotalFacturacion();
			ejemploListarFacturas();
		}
	}

	private static void ejemploLogin() {
		try {
			Cliente cli = clienteServicio.loginCliente("usr1", "4321");
			System.out.println("Autenticado " + cli);
		} catch (Exception e) {
			System.out.println("Error en autenticación: " + e.getMessage());
		}

	}

	private static void ejemploCambioContrasenya() {
		try {
			clienteServicio.cambiaContrasenya(1, "4321");
			System.out.println("Contraseña cambiada con éxito (cliente 1)");
		} catch (Exception e) {
			System.out.println("Error cambiando contraseña: " + e.getMessage());
		}

	}

	private static void ejemploCambioPerfil() {
		try {
			Cliente cli = clienteServicio.obtenerCliente(2);
			cli.setNombre("Mateo");
			clienteServicio.cambiaPerfil(cli);
			System.out.println("Perfil cambiado satisfactoriamente: " + cli);
		} catch (Exception e) {
			System.out.println("Error cambiando perfil: " + e.getMessage());
		}

	}

	private static void ejemploRegistro() {
		try {
			Cliente nuevoCliente = new Cliente("Aa Aaaa", "C/ Aa", "usr_e", "1234");
			clienteServicio.registraCliente(nuevoCliente);
			System.out.println("Registro nuevo usuario OK: " + nuevoCliente);
		} catch (Exception e) {
			System.out.println("Error registrando usuario: " + e.getMessage());
		}

	}

	private static void ejemploBusquedaPorNombre() {
		try {
			System.out.println("Busqueda por nombre 'm'");
			List<Cliente> listaClientes = clienteServicio.buscaPorNombre("m");
			for (Cliente c : listaClientes) {
				System.out.println(c);
			}
		} catch (Exception e) {
			System.out.println("Error en búsqueda: " + e.getMessage());
		}

	}

	private static void ejemploMostrarTotalFacturacion() {
		int clienteId = 1;
		double total;
		try {
			System.out.print("El total facturado del cliente " + clienteId + " es: ");
			total = clienteServicio.calculaTotalFacturado(clienteId);
			System.out.println(total + " €");
		} catch (Exception e) {
			System.out.println("Error calculando total facturacion: " + e.getMessage());
		}

	}

	private static void ejemploListarFacturas() {
		int clienteId = 1;
		try {
			Cliente cliente = clienteServicio.obtenerCliente(clienteId);
			clienteServicio.cargaFacturas(cliente);
			Set<Factura> listaFacturas = cliente.getFacturas();
			// mostramos solo las 10 primeras
			int i = 0;
			for (Factura f : listaFacturas) {
				System.out.println(f);
				if (i++ == 10) {
					break;
				}
			}
		} catch (Exception e) {
			System.out.println("Error leyendo facturas: " + e.getMessage());
		}

	}

}
